from unittest import TestCase

from SecuenciaNumeros import SecuenciaNumeros

class SecuenciaNumerosTest(TestCase):
    def test_numero_elementos_string_vacio(self):
        self.assertEqual(SecuenciaNumeros().numeroElementos(""),0,"String vacio")

    def test_numero_elementos_un_numero(self):
        self.assertEqual(SecuenciaNumeros().numeroElementos("2"), 1, "1 numero")

    def test_numero_elementos_dos_numeros(self):
        self.assertEqual(SecuenciaNumeros().numeroElementos("2,3"), 2, "2 numeros")

    def test_numero_elementos_n_numeros(self):
        self.assertEqual(SecuenciaNumeros().numeroElementos("2,3,5,6,3,7"), 6, "N numeros")

    def test_numero_elementos_minimo_strvacio(self):
        self.assertEqual(SecuenciaNumeros().calculaMinimo(""), 0, "String vacio")

    def test_numero_elementos_minimo_unNumero(self):
        self.assertEqual(SecuenciaNumeros().calculaMinimo("2"), 2, "1 numero")

    def test_numero_elementos_minimo_dosNumeros(self):
        self.assertEqual(SecuenciaNumeros().calculaMinimo("2,1"), 1, "numero minimo con 2 numeros")

    def test_numero_elementos_minimo_n_Numeros(self):
        self.assertEqual(SecuenciaNumeros().calculaMinimo("7,9,5,8,3"), 3, "numero minimo con multiples numeros")

    def test_numero_elementos_maximo_strvacio(self):
        self.assertEqual(SecuenciaNumeros().calculaMaximo(""), 0, "String vacio")

    def test_numero_elementos_maximo_unNumero(self):
        self.assertEqual(SecuenciaNumeros().calculaMaximo("2"), 2, "1 numero")

    def test_numero_elementos_maximo_dosNumeros(self):
        self.assertEqual(SecuenciaNumeros().calculaMaximo("2,1"), 2, "numero maximo con 2 numeros")

    def test_numero_elementos_maximo_n_Numeros(self):
        self.assertEqual(SecuenciaNumeros().calculaMaximo("7,3,5,8,9"), 9, "numero maximo con multiples numeros")

    def test_numero_elementos_promedio_strVacio(self):
        self.assertEqual(SecuenciaNumeros().calculaPromedio(""), 0, "Promedio con cadena vacia")

    def test_numero_elementos_promedio_unNumero(self):
        self.assertEqual(SecuenciaNumeros().calculaPromedio("3"), 3, "Promedio con un numero")

    def test_numero_elementos_promedio_dosNumeros(self):
        self.assertEqual(SecuenciaNumeros().calculaPromedio("3,9"), 6, "Promedio con dos numeros")

    def test_numero_elementos_promedio_n_Numeros(self):
        self.assertEqual(SecuenciaNumeros().calculaPromedio("11,5,3,9"), 7, "Promedio con multiples numeros")

    def test_arreglo_resultados(self):
        self.assertEqual(SecuenciaNumeros().procesarEstadisiticas("5,2,6,3"), [4,2,6,4], "Arreglo resultados")