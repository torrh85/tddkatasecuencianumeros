class SecuenciaNumeros:

    def procesarEstadisiticas(self,cadena):
        numeroelementos = self.numeroElementos(cadena)
        minimo = self.calculaMinimo(cadena)
        maximo = self.calculaMaximo(cadena)
        promedio = self.calculaPromedio(cadena)

        return [numeroelementos,minimo,maximo,promedio]


    def numeroElementos(self, cadena):
        if cadena == "":
            return 0
        else:
            numeros = cadena.split(",")
            return len(numeros)


    def calculaMinimo (self, cadena):
        if cadena == "":
            return 0
        elif "," in cadena:
            numeros = cadena.split(",")
            min = int(numeros[0])
            for num in numeros:
                if int(num) < min:
                    min = int(num)
            return min
        else:
            return int(cadena)

    def calculaMaximo(self, cadena):
        if cadena == "":
            return 0

        elif "," in cadena:
            numeros = cadena.split(",")
            max = int(numeros[0])
            for num in numeros:
                if int(num) > max:
                    max = int(num)
            return max

        return int(cadena)

    def calculaPromedio(self, cadena):
        if cadena == "":
            return 0

        elif "," in cadena:
            numeros = cadena.split(",")
            total = len(numeros)
            sum = 0
            for num in numeros:
                sum = sum + int(num)

            prom = sum / total

            return prom
        else:
            return int(cadena)